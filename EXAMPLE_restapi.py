import pendulum
import json

from airflow import DAG
from airflow.providers.http.sensors.http import HttpSensor
from airflow.providers.http.operators.http import SimpleHttpOperator
from airflow.operators.python import PythonOperator


def save_posts(ti) -> None:
    posts=ti.xcom_pull(task_ids=["get_posts"])
    with open("/mnt/c/Users/visdez/airflow_playground/posts.json", "w") as f:
        json.dump(posts[0], f)

with DAG(
    dag_id='EXAMPLE_restapi',
    schedule='@daily',
    start_date=pendulum.datetime(2023,4,16, tz="Europe/Stockholm"),
    tags=['example']
) as dag:

    api_acitve = HttpSensor(
        task_id="api_active",
        http_conn_id="api_posts",
        endpoint="posts/"
    )

    get_posts = SimpleHttpOperator(
        task_id="get_posts",
        http_conn_id="api_posts",
        endpoint="posts/",
        method="GET",
        response_filter=lambda response: json.loads(response.text),
        log_response=True,
    )

    save = PythonOperator(
        task_id="save_posts",
        python_callable=save_posts
    )