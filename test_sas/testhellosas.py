# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.

from datetime import datetime
from airflow import DAG
from sas_airflow_provider.operators.sas_studioflow import SASStudioFlowOperator

dag = DAG('testhellosas', description='Executing Studio Flow for demo purposes',
          schedule_interval='0 12 * * *',
          start_date=datetime(2022, 6, 1), catchup=False)



task1 = SASStudioFlowOperator(task_id='test_sas_01.flw',
                              flow_path_type='compute',
                              flow_path='/shared-data/Viktor_SAS/Airflow/test_sas_01.flw',
                              flow_exec_log=True,
                              compute_context="SAS Studio compute context",
                              flow_codegen_init_code=False,
                              flow_codegen_wrap_code=False,
                              dag=dag)

task2 = SASStudioFlowOperator(task_id='test_sas_02.flw',
                              flow_path_type='compute',
                              flow_path='/shared-data/Viktor_SAS/Airflow/test_sas_02.flw',
                              flow_exec_log=True,
                              compute_context="SAS Studio compute context",
                              flow_codegen_init_code=False,
                              flow_codegen_wrap_code=False,
                              dag=dag)

task1 >> task2