import pendulum

from airflow import DAG
from airflow.sensors.external_task import ExternalTaskSensor
from airflow.operators.bash import BashOperator

with DAG(
    dag_id="EXAMPLE_external_task_sensor",
    schedule= "@monthly",
    start_date=pendulum.datetime(2023,4,10, tz="Europe/Stockholm"),
    tags=['example'],
) as dag:
    
    task_sensor = ExternalTaskSensor(
        task_id="task_sensor",
        external_dag_id="DEMO_airflowchart.py",
        external_task_id="library_prep",
        timeout=300,
        allowed_states=["success"],
    )

    message = BashOperator(
        task_id="message",
        bash_command="echo external dag ran succesfully, continue",
    )

    task_sensor >> message