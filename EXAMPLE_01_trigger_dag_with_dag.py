
import pendulum

from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.operators.trigger_dagrun import TriggerDagRunOperator

with DAG(
    dag_id="EXAMPLE_trigger_dag_with_dag_01",
    start_date=pendulum.datetime(2023,4,10, tz="Europe/Stockholm"),
    catchup=False,
    schedule="@once",
    tags=["example"],
) as dag:
    start = BashOperator(
        task_id="start",
        bash_command="echo Start of trigger dag",
    )

    trigger = TriggerDagRunOperator(
        task_id="trigger",
        trigger_dag_id="EXAMPLE_triggerd_by_DAG",
        wait_for_completion=False,
    )

start >> trigger
