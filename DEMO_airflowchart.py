import pendulum
from airflow import DAG
from sas_airflow_provider.operators.sas_studioflow import SASStudioFlowOperator
from airflow.operators.empty import EmptyOperator

dag = DAG('DEMO_airflow_chart', description='Executing Studio Flow for demo purposes',
          start_date=pendulum.datetime(2023, 4, 1, tz="Europe/Stockholm"), 
          catchup=False,
          schedule="@monthly",
          tags=['example', 'SAS'])

dummy = EmptyOperator(
    task_id="empty",
    dag=dag,
)

library_prep = SASStudioFlowOperator(task_id='library_prep',
                              flow_path_type='compute',
                              flow_path='/shared-data/Viktor_SAS/Airflow/01_load_library.flw',
                              flow_exec_log=True,
                              compute_context="SAS Studio compute context",
                              flow_codegen_init_code=False,
                              flow_codegen_wrap_code=False,
                              dag=dag)

number_prep = SASStudioFlowOperator(task_id='number_prep',
                              flow_path_type='compute',
                              flow_path='/shared-data/Viktor_SAS/Airflow/02_prep_numbers.flw',
                              flow_exec_log=True,
                              compute_context="SAS Studio compute context",
                              flow_codegen_init_code=False,
                              flow_codegen_wrap_code=False,
                              dag=dag)

table_prep = SASStudioFlowOperator(task_id='table_prep',
                              flow_path_type='compute',
                              flow_path='/shared-data/Viktor_SAS/Airflow/03_prep_table.flw',
                              flow_exec_log=True,
                              compute_context="SAS Studio compute context",
                              flow_codegen_init_code=False,
                              flow_codegen_wrap_code=False,
                              dag=dag)

final_execution = SASStudioFlowOperator(task_id='final_execution',
                              flow_path_type='compute',
                              flow_path='/shared-data/Viktor_SAS/Airflow/05_final_execution.flw',
                              flow_exec_log=True,
                              compute_context="SAS Studio compute context",
                              flow_codegen_init_code=False,
                              flow_codegen_wrap_code=False,
                              dag=dag)

side_task = SASStudioFlowOperator(task_id='side_task',
                              flow_path_type='compute',
                              flow_path='/shared-data/Viktor_SAS/Airflow/04_side_task.flw',
                              flow_exec_log=True,
                              compute_context="SAS Studio compute context",
                              flow_codegen_init_code=False,
                              flow_codegen_wrap_code=False,
                              dag=dag,)

dummy >> [library_prep, number_prep]  >> final_execution

side_task >> table_prep >> final_execution