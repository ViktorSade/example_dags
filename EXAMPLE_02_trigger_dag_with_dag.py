import pendulum

from airflow import DAG
from airflow.operators.bash import BashOperator


with DAG(
    dag_id="EXAMPLE_trigger_dag_with_dag_02",
    start_date=pendulum.datetime(2023,4,10, tz="Europe/Stockholm"),
    schedule=None,
    tags=['example']
    ) as dag:
    trigger_confirm = BashOperator(
        task_id="trigger_confirm",
        bash_command="echo This DAGs was triggered by another DAG",
    )