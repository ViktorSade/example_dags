import pendulum

from airflow import DAG
from airflow.sensors.filesystem import FileSensor
from airflow.operators.bash import BashOperator

with DAG(
    dag_id ="EXAMPLE_file_sensor",
    schedule="@weekly",
    start_date=pendulum.datetime(2023,4,10, tz="Europe/Stockholm"),
    tags=['example'],
    catchup=False,
) as dag:
## This filetrigger will wait actively on a worker node for the file to appear
    filesensor = FileSensor(task_id="filesensor", 
                            filepath="/mnt/c/Users/visdez/airflow_playground/filetrigger.txt",
                            fs_conn_id="fs_default",
                            )
    message = BashOperator(
        task_id="message",
        bash_command="echo fil mottagen > /mnt/c/Users/visdez/airflow_playground/filetrigger.txt"
    )

filesensor >> message