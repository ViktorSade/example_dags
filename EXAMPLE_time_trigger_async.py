
import datetime

import pendulum

from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.sensors.time_delta import TimeDeltaSensorAsync

with DAG(
    dag_id="EXAMPLE_time_trigger_async",
    schedule="@daily",
    start_date=pendulum.datetime(2023, 4, 19,10,30, tz="Europe/Stockholm"),
    tags=['example'],
    catchup=False,

) as dag:
    start_message = BashOperator(
        task_id="start_message",
        bash_command="echo started job",
    )

    delay = TimeDeltaSensorAsync(
        task_id="delay",
        delta=datetime.timedelta(seconds=30),
    )
    
    finish_message = BashOperator(
        task_id="finish_message",
        bash_command="echo finished job",
    )

start_message >> delay >> finish_message