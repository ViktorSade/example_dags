# Example by Viktor Säde: 
# Contains two DAGs
# DAG 1: Updates a local file
# DAG 2: Runs upon sensing an update on the same file, this DAG does not need the first to run neccessarily. The update can be both updating and creating the file

import pendulum

from airflow import DAG
from airflow.datasets import Dataset
from airflow.operators.bash import BashOperator

with DAG(
    dag_id="EXAMPLE_run_on_data_update_01",
    catchup=False,
    start_date=pendulum.datetime(2023,5,2, tz="Europe/Stockholm"),
    tags=['example'],
    schedule = None,
) as dag:
    update_file = BashOperator(
        bash_command="echo FILE UPDATE >> /mnt/c/Users/visdez/airflow_playground/start_dag.csv",
        task_id="Update_file",
        outlets=[Dataset("/mnt/c/Users/visdez/airflow_playground/start_dag.csv")], # outlets required for Datset dag to run.
    )

with DAG(
    dag_id='EXAMPLE_run_on_data_update_02',
    catchup=False,
    start_date=pendulum.datetime(2023,5,2, tz="Europe/Stockholm"),
    tags=['example'],
    schedule= [Dataset("/mnt/c/Users/visdez/airflow_playground/start_dag.csv")], # Dataset according to https://airflow.apache.org/docs/apache-airflow/2.6.0/authoring-and-scheduling/datasets.html
) as dag:
    update_confirmation = BashOperator(
        bash_command="echo FILE UPDATE RECEIVED >> /mnt/c/Users/visdez/airflow_playground/start_dag.csv",
        task_id="Update_confirmation",
    )


